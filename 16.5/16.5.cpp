﻿#include <iostream>
using namespace std;

int main()
{
    const int n = 6;
    int array[n][n];
    int sum = 0;
    int Day;

    // Ввод текущего числа календаря
    cout << "Enter Day" << endl;
    cin >> Day;

    cout << endl;

    int ost = Day % n;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j] << " ";
            if (ost == i)
            {
                sum += array[i][j];
            }
        }
        cout << "\n";
    }

    cout << endl << sum << endl;
  
}